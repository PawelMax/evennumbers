package pl.SDA;

public class Collection {

    private int givenNumber;
    private int sumOfNumbers = 0;
    private int sumOfAllNumbers = 0;
    private float averageNumber;

    /*public Collection(int givenNumber) {
        this.givenNumber = givenNumber;
    }

    public Collection() {

    }*/

    public int getGivenNumber() {
        return givenNumber;
    }

    public void setGivenNumber(int givenNumber) {
        this.givenNumber = givenNumber;
    }

    public int getSumOfNumbers() {
        return sumOfNumbers;
    }

    public void setSumOfNumbers(int sumOfNumbers) {
        this.sumOfNumbers = sumOfNumbers;
    }

    public int getSumOfAllNumbers() {
        return sumOfAllNumbers;
    }

    public void setSumOfAllNumbers(int sumOfAllNumbers) {
        this.sumOfAllNumbers = sumOfAllNumbers;
    }

    public float getAverageNumber() {
        return averageNumber;
    }

    public void setAverageNumber(float averageNumber) {
        this.averageNumber = averageNumber;
    }
}
